package fr.cnam.foad.nfa035.fileutils.simpleaccess.test;

import java.io.IOException;

public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {
	
    @Override
    public final void deserialize(M media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}

