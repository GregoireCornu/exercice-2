package fr.cnam.foad.nfa035.fileutils.simpleaccess.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class AbstractImageFrameMedia<T> {
	/**
	 * @channel objet contenant le media
	 */
	private T channel;
       
	/**
	 * Constructeur par défaut
	 */
    protected AbstractImageFrameMedia() {
    }
    /**
     * Méthode pour obtenir l'objet contenant le media
     * @return channel
     */
    public T getChannel() {
        return channel;
    }
    
    /**
     * Méthode pour définir l'objet contenant le media
     * @return channel
     */
    public void setChannel(T channel) {
        this.channel = channel;
    }
    
    /**
     * Constructeur
     * @param attribut désignant l'objet contenant le media
     */
    AbstractImageFrameMedia(T channel){
        this.channel = channel;
    }
    
    /**
     *  Méthode abstraite de sortie pour l'image encodée
     * @throws IOException
     */
    public abstract OutputStream getEncodedImageOutput() throws IOException;
    
    /**
     *  Méthode abstraite d'entrée pour l'image encodée
     * @throws IOException
     */
    public abstract InputStream getEncodedImageInput() throws IOException;
}

